const fs = require("fs");

const fileName = "dictionary.json",
    encoding = "utf8";


const createFile = (file) => {
    fs.writeFile(file, JSON.stringify({}), encoding, () => {
        //console.log("file created !");
    });
};

const readFile = (callback) => {
    if (!fs.existsSync(fileName)) {
        createFile(fileName);
    }

    fs.readFile("dictionary.json", "utf8", function readFileCallback(err, data) {
        if (err) {
            console.log(err);
        } else {
            let obj = JSON.parse(data);
            callback(obj);
        }
    });
};


const add = (args) => {

    const key = args[1],
        value = args[2];

    readFile(function (data) {

        delete data[key];
        data[key] = value;

        fs.writeFile(fileName, JSON.stringify(data), encoding, function () {
            console.log("key added !");
        });
    });
};

const list = () => {
    readFile(function (object) {

        if (!Object.keys(object).length) {
            console.log("no keys found !");
        }

        for (const key in object) {
            if (object.hasOwnProperty(key)) {
                const element = object[key];
                console.log(`key:${key}, value:${element}`);
            }
        }
    });
};

const get = (args) => {

    const key = args[1];

    readFile(function (data) {
        if (data[key]) {
            console.log(data[key])
        } else {
            console.log(`key ${key} not found!`);
        }
    });
};

const remove = (args) => {

    const key = args[1];

    readFile(function (data) {

        if (!data[key]) {
            console.log("key not found !");
        } else {
            delete data[key];

            fs.writeFile(fileName, JSON.stringify(data), encoding, function () {
                console.log("key removed !")
            });
        }
    });
};

const clear = () => {
    fs.writeFile(fileName, JSON.stringify({}), encoding, function () {
        console.log("file cleared !");
    });
};


const main = () => {

        const args = process.argv.slice(2);

        if (args.length > 0) {

            const operations = {
                add,
                list,
                get,
                remove,
                clear
            };

            console.log("Enter command, please:");
            operations[args[0]](args);

        } else {
            console.log("please enter a valid command !");
        }
};

main();